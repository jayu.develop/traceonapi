<?php

$app->get('/', 'HomeController:index')->setName('home');
//--
$app->get('/poppularWeeklyController/poppularWeekly', 'poppularWeeklyController:poppularWeekly')->setName('poppularWeekly');
$app->get('/popularMonthlyController/popularMonthly', 'popularMonthlyController:popularMonthly')->setName('popularMonthly');
$app->get('/newReleasesController/newReleases', 'newReleasesController:newReleases')->setName('newreleases');
$app->get('/justReturnController/justReturn', 'justReturnController:justReturn')->setName('justReturn');

$app->get('/checkstatusController/checkstatus/{re_code}', 'checkstatusController:checkstatus')->setName('checkstatus');
$app->get('/checkinController/checkin/{re_code}', 'checkinController:checkin')->setName('checkin');
//$app->get('/checkpatronController/checkpatron/{member_id}', 'checkpatronController:checkpatron');
$app->get('/checkpatronController/checkpatron/{PatronID}', 'checkpatronController:checkpatron')->setName('checkpatron');


$app->get('/checkoutController/checkout/{member_cardID},{re_code}', 'checkoutController:checkout')->setName('checkout');
$app->get('/checkout_againController/checkout_again/{member_cardID},{re_code}', 'checkout_againController:checkout_again')->setName('checkout_again');

//test
$app->post('/testController/test', 'testController:test')->setName('test');
