<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use PDO;

class justReturnController extends Controller
{
	public function justReturn(ServerRequestInterface $request, ResponseInterface $response)
    {
        error_reporting(E_ALL & ~E_NOTICE);
        $temparray = array();
        $sql_just_return = $this->container->db->query("SELECT 
        resource_code ,re_image ,date_return 
        FROM view_just_return");
        $sql_just_return->execute();
        
        foreach($resultsql_just_return = $sql_just_return->fetchAll(PDO::FETCH_OBJ) AS $row) {

            $temparray[] = array(
                'resource_code' => $row->resource_code, 
                're_image' => $row->re_image, 
                'date_return' => $row->date_return
            );

        }
        
        $response = $this->response->withJson($temparray);
        return $response;


    }
    
}