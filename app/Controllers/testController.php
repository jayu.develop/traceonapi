<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use PDO;
use App\Controllers\Controller;

class testController extends Controller
{
    public function test(ServerRequestInterface $request, ResponseInterface $response)
    {
        if($_POST['command'] == "newReleases"){
            return $response->withRedirect($this->container->router->pathFor('newreleases'));
        }
        else if($_POST['command'] == "justReturn"){
            return $response->withRedirect($this->container->router->pathFor('justReturn'));
        }
        else if($_POST['command'] == "popularWeekly"){
            return $response->withRedirect($this->container->router->pathFor('popularWeekly'));
        }
        else if($_POST['command'] == "popularMonthly"){
            return $response->withRedirect($this->container->router->pathFor('popularMonthly'));
        }
        else if($_POST['re_code'] != "" && $_POST['command'] == "checkstatus"){

            $dataResult = array(
                're_code' => $_POST["re_code"]
            );
            return $response->withRedirect($this->container->router->pathFor('checkstatus', $dataResult));
        }
        else if($_POST['PatronID'] != "" && $_POST['command'] == "checkpatron"){

            $dataResult = array(
                'member_cardID' => $_POST["PatronID"]
            );
            return $response->withRedirect($this->container->router->pathFor('checkpatron', $dataResult));
        }
        else if($_POST['re_code']!= "" && $_POST['command'] == "checkin"){

            $dataResult = array(
                're_code' => $_POST["re_code"]
            );
            return $response->withRedirect($this->container->router->pathFor('checkin', $dataResult));
        }
        else if($_POST['PatronID'] != "" && $_POST['re_code'] != "" && $_POST['command'] == "checkout"){

            $dataResult = array(
                'member_cardID' => $_POST["PatronID"],
                're_code' => $_POST["re_code"]
            );
            return $response->withRedirect($this->container->router->pathFor('checkout', $dataResult));
        }
        else if($_POST['PatronID'] != "" && $_POST['re_code'] != "" && $_POST['command'] == "checkout_again"){
            
            $dataResult = array(
                'member_cardID' => $_POST["PatronID"],
                're_code' => $_POST["re_code"]
            );
            return $response->withRedirect($this->container->router->pathFor('checkout_again', $dataResult));
        }
        else
        {
            return $response->withRedirect($this->container->router->pathFor('home'));
        }
     
        
    }
    
}