<?php

namespace App\Controllers;

use App\Models\User;
use Slim\Views\Twig as View;

class HomeController extends Controller
{
    public function index($request, $response)
    {
        //return $this->view->render($response, 'home2.php');
        return $this->view->render($response, 'home.twig');
        //require __DIR__ . '/../../template/home.php';
    }
}
