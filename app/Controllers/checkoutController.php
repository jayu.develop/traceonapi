<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;

use PDO;

class checkoutController extends Controller
{
    public function DateDiff($strDate1,$strDate2)
    {
        return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );
    }

	public function checkout(ServerRequestInterface $request, ResponseInterface $response)
    {
        error_reporting(E_ALL & ~E_NOTICE);
        //strMem
        $member_cardID = $request->getAttribute('member_cardID');
        $re_code = $request->getAttribute('re_code');

        $strMem = $this->container->db->query("SELECT 
        member.member_id,member.member_status, member.member_expire, 
        privilege.number_day, privilege.number_resource 
        FROM member 
        INNER JOIN privilege ON member.member_type = privilege.member_type 
        WHERE member.member_cardID = '$member_cardID'");
        $strMem->execute();
        $resultstrMem = $strMem->fetchAll(PDO::FETCH_OBJ);

        //selectCheckBorrow
        $selectCheckBorrow = $this->container->db->query("SELECT status_resource,id_borrow 
        FROM borrow 
        WHERE resource_code  = '$re_code' ORDER BY id_borrow DESC LIMIT 1");
        $selectCheckBorrow->execute();
        $resultCheckBorrow = $selectCheckBorrow->fetchAll(PDO::FETCH_OBJ);

        //strResource
        $strResource = $this->container->db->query("SELECT re_title, re_code, re_image 
        FROM resource 
        WHERE re_code  = '$re_code'");
        $strResource->execute();
        $resultstrResource = $strResource->fetchAll(PDO::FETCH_OBJ);

        //strBorrowCount
        $strBorrowCount = $this->container->db->query("SELECT status_resource 
        FROM borrow  
        WHERE member_id = '".$resultstrMem[0]->member_id."' AND (status_resource = '1')");
        $strBorrowCount->execute(); 
        $resultstrBorrowCount = $strBorrowCount->rowCount(PDO::FETCH_OBJ);
        
        //ค่าปรับ
        $strBorrow = $this->container->db->query("SELECT borrow.date_return, borrow.date_end, 
        borrow.status_fine 
        FROM borrow 
        LEFT JOIN resource ON borrow.resource_code= resource.re_code 
        WHERE borrow.member_id = '".$resultstrMem[0]->member_id."' 
        AND (borrow.status_resource = '1' OR borrow.status_fine = '1') 
        AND  borrow.date_end < CURDATE()");
        $strBorrow->execute();

        //ตัวแปรค่าปรับ
        $strSQLFine = $this->container->db->query("SELECT rate_fine FROM rate_fine");
        $strSQLFine->execute();
        $resultSQLFine = $strSQLFine->fetchAll(PDO::FETCH_OBJ);
        $rateFine = $resultSQLFine[0]->rate_fine;

        $sumFine = 0;
        foreach($resultstrBorrow = $strBorrow->fetchAll(PDO::FETCH_OBJ) AS $row){
            if($row->date_return != '0000-00-00' && $row->status_fine == '1'){
                $dateDiff = $this->DateDiff($row->date_end, $row->date_return);
                $valueFine = $dateDiff * $rateFine;
            }else{
                $dateDiff = $this->DateDiff($row->date_end, date('Y-m-d'));
                $valueFine = $dateDiff * $rateFine;                                        
            } 

            $sumFine += $valueFine;
        }

        
        $due = date("Y-m-d", mktime(date("H")+0, date("i")+0, date("s")+0, date("m")+0, date("d")+$resultstrMem[0]->number_day, date("Y")+0));
        $expire = $this->DateDiff(date('Y-m-d'), $resultstrMem[0]->member_expire); 
        
        if(isset($resultstrResource) && isset($resultstrMem) && $resultstrMem[0]->member_status == '1' && $expire > '30' 
            && $resultstrMem[0]->number_resource > $resultstrBorrowCount && ($sumFine == '' || $sumFine == '0') 
            && $resultCheckBorrow[0]->status_resource != '1')
        {
            $today = date("Y-m-d");
            $objQuery = $this->container->db->query("INSERT INTO borrow 
            (resource_code, member_id, date_start, date_end, status_resource) 
            VALUES ('".$re_code."','".$resultstrMem[0]->member_id."','".$today."','".$due."','1')"); 
            $objQuery->execute();

                $status = "success";
        }else{
                $status = "false";
        }

        if($status == "success"){
            $temparray = array();
            $temparray[] = array(
                'error' => 0, 
                'error_description' => '', 
                'media_name' => $resultstrResource[0]->re_title, 
                'media_code' => $resultstrResource[0]->re_code, 
                're_image' => $resultstrResource[0]->re_image, 
                'due' => $due, 
                'status' => $status 
            );
    
            $response = $this->response->withJson($temparray);
            return $response;  
        }

        else if($status == "false")
        {
            if(!isset($resultstrMem)){ $error_description = "ไม่มีข้อมูลผู้ใช้ในระบบ"; }
            
            else if($resultstrMem[0]->member_status == '0'){ $error_description = "สมาชิกโดนระงับการใช้งาน"; }
            else if($expire < '0'){ $error_description = "บัตรหมดอายุ"; }
            else if($expire <= '30'){ $error_description = "บัตรใกล้หมดอายุ"; }
            else if($sumFine > '0'){ $error_description = "มีค่าปรับคงค้าง"; }
            else if(!isset($resultstrResource)){ $error_description = "ไม่มีทรัพยากรในระบบ"; }
            else if($resultCheckBorrow[0]->status_resource == '1'){ $error_description = "หนังสือถูกยืมอยู่"; }
            else if($resultstrMem[0]->number_resource <= $resultstrBorrowCount){ $error_description = "ยืมหนังสือเกินกำหนด"; }
        
                $temparray = array();
                $temparray[] = array(
                    'error' => 1, 
                    'error_description' => $error_description, 
                    'media_name' => $resultstrResource[0]->re_title, 
                    'media_code' => $resultstrResource[0]->re_code, 
                    're_image' => $resultstrResource[0]->re_image
                );
        
                $response = $this->response->withJson($temparray);
                return $response;

        }

         
        
    }
}