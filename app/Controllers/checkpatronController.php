<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use fuctionOutsite;
use PDO;

class checkpatronController extends Controller
{
    public function DateDiff($strDate1,$strDate2)
    {
        return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
    }

    public function CheckFine($memid)
    {
        //ตัวแปรค่าปรับ
        $strSQLFine = $this->container->db->query("SELECT rate_fine FROM rate_fine");
        $strSQLFine->execute();
        $resultSQLFine = $strSQLFine->fetchAll(PDO::FETCH_OBJ);
        $rateFine = $resultSQLFine[0]->rate_fine;

        $strBorrow = $this->container->db->query("SELECT 
        borrow.date_return, borrow.date_end, borrow.status_fine, 
        resource.re_code, resource.re_title,resource.re_image 
        FROM borrow 
        LEFT JOIN resource ON borrow.resource_code = resource.re_code 
        WHERE borrow.member_id = '".$memid."' AND (borrow.status_resource = '1' OR borrow.status_fine = '1')
        AND  borrow.date_end < CURDATE()");
        $strBorrow->execute();
        //$intNumField = mysqli_num_fields($strBorrow);
        
        $resultArray = array();
        foreach($resultstrBorrow = $strBorrow->fetchAll(PDO::FETCH_OBJ) AS $row) {
            if($row->date_return != '0000-00-00' && $row->status_fine == '1'){
                $dateDiff = $this->DateDiff($row->date_end, $row->date_return);
                $valueFine = $dateDiff * $rateFine;
            }else{								  
                $dateDiff = $this->DateDiff($row->date_end, date('Y-m-d'));
                $valueFine = $dateDiff * $rateFine;                                        
            }  
            $arrCol = array(
                'barcode' => $row->re_code,
                'end' => $row->date_end,
                'overdue' => $dateDiff,
                'fine' =>$valueFine, 
                'title' => $row->re_title,
                're_image' => $row->re_image
            );
            array_push($resultArray,$arrCol);
        }
        
        return $resultArray;

    }
    
    public function checkOutlist($memid)
    {
        $strBorrow = $this->container->db->query("SELECT 
        borrow.date_start, borrow.date_end, resource.re_code, resource.re_title, resource.re_image 
        FROM borrow 
        LEFT JOIN resource ON borrow.resource_code= resource.re_code 
        WHERE borrow.member_id = '".$memid."' AND borrow.status_resource = '1'");
        $strBorrow->execute();
        //$intNumField->fetchAll($strBorrow);
        $resultArray = array();
        foreach($resultBorrow = $strBorrow->fetchAll(PDO::FETCH_OBJ) AS $row ){
            $arrCol = array('barcode' => $row->re_code, 'title' => $row->re_title, 
            'start' => $row->date_start, 'end' => $row->date_end, 're_image' => $row->re_image);
            array_push($resultArray,$arrCol);
        }
        return $resultArray;
        
    }
    
	public function checkpatron(ServerRequestInterface $request, ResponseInterface $response)
    {   
        error_reporting(E_ALL & ~E_NOTICE);
        // $member_id = $request->getAttribute('member_id');
        $PatronID = $request->getAttribute('PatronID');

        if(strlen($PatronID) == "5"){
            //if(!empty($_SESSION['errors'])) // close Notice
            $sqlPatron1 =" WHERE 1 AND (member.member_id = '".$PatronID."')";
        }else{
            //if(!empty($_SESSION['errors'])) // close Notice
            $sqlPatron1 =" WHERE 1 AND (member.member_cardID = '".$PatronID."')";
            }

            $sqlPatron = $this->container->db->query("SELECT 
            member.member_name, member.member_lastname, 
            member.member_cardID, member.member_id, 
            member.member_pincard, member.member_type, 
            member.member_image, privilege.number_resource, 
            privilege.number_day 
            FROM privilege 
            LEFT JOIN member ON privilege.member_type = member.member_type $sqlPatron1");

            $sqlPatron->execute();
            $resultPatron = $sqlPatron->fetchAll(PDO::FETCH_OBJ);
            
            if(!$resultPatron){
                $chk_member = "false";
            }else{
                $chk_member = "true";
            }
        
        //ตัวแปรค่าปรับ
        $strSQLFine = $this->container->db->query("SELECT rate_fine FROM rate_fine");
        $strSQLFine->execute();
        $resultSQLFine = $strSQLFine->fetchAll(PDO::FETCH_OBJ);
        $rateFine = $resultSQLFine[0]->rate_fine;
        

        //เช็คค่า่ปรับ
        $strBorrow = $this->container->db->query("SELECT 
        borrow.date_return, borrow.date_end, borrow.status_fine 
        FROM borrow 
        LEFT JOIN resource ON borrow.resource_code = resource.re_code 
        WHERE borrow.member_id = '".$resultPatron[0]->member_id."' 
        AND (borrow.status_resource = '1' OR borrow.status_fine = '1') 
        AND  borrow.date_end < CURDATE()");
        $strBorrow->execute();
        $fine = 0;
        foreach($resultstrBorrow = $strBorrow->fetchAll(PDO::FETCH_OBJ) AS $row) {
            if($row->date_return != '0000-00-00' && $row->status_fine == '1'){
                $dateDiff = $this->DateDiff($row->date_end, $row->date_return);
                $valueFine = $dateDiff * $rateFine;
            }else{
                $dateDiff = $this->DateDiff($row->date_end, date('Y-m-d'));//edit date
                $valueFine = $dateDiff * $rateFine;
            }
            $fine += $valueFine;
        }
        
        if($fine > 0){
            $chk_permit = "false";
        }else{
            $chk_permit = "true";
        }

        //Overdue
        $strOverdue = $this->container->db->query("SELECT resource_code FROM borrow 
        WHERE member_id = '".$resultPatron[0]->member_id."' 
        AND status_resource = '1'
        AND date_end < CURDATE()");
        $strOverdue->execute();
        $resultOverdue = $strOverdue->rowCount(PDO::FETCH_OBJ);

        //Checkedout
        $strCheckedout = $this->container->db->query("SELECT resource_code FROM borrow 
        WHERE member_id = '".$resultPatron[0]->member_id."' 
        AND status_resource = '1'");
        $strCheckedout->execute();
        $resultCheckdout = $strCheckedout->rowCount(PDO::FETCH_OBJ);

        //list
        $resultCus = $this->CheckFine($resultPatron[0]->member_id);
        $resultOut = $this->checkOutlist($resultPatron[0]->member_id);
        
        $error = '0';
        $error_description = "";

    
        $temparray = array();
        $temparray[] = array(
            'error' => $error, 
            'error_description' => $error_description, 
            'chk_member' => $chk_member, 
            'member_cardID' => $resultPatron[0]->member_cardID, 
            'member_id' => $resultPatron[0]->member_id, 
            'name' => $resultPatron[0]->member_name." ".$resultPatron[0]->member_lastname, 
            'member_pincard' => $resultPatron[0]->member_pincard, 
            'member_type' => $resultPatron[0]->member_type, 
            'member_image' => $resultPatron[0]->member_image, 
            'maxcheckout' => $resultPatron[0]->number_resource, 
            'fine' => $fine, 
            'fine_list' => $resultCus, 
            'chk_permit' => $chk_permit, 
            'checkout_list' => $resultOut,  
            'num_overdue' => $resultOverdue, 
            'num_checkedout' => $resultCheckdout
        );

        $response = $this->response->withJson($temparray);
        return $response;
    }
}
