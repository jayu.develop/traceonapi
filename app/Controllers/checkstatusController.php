<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use PDO;
use App\Controllers\Controller;

class checkstatusController extends Controller
{
	public function checkstatus(ServerRequestInterface $request, ResponseInterface $response)
    {
        error_reporting(E_ALL & ~E_NOTICE);
        $re_code = $request->getAttribute('re_code');
        $strReso = $this->container->db->query("SELECT 
        resource.re_code, resource.re_title, 
        resource.re_auther, resource.re_topic, 
        resource.re_note, resource.re_isbn, 
        resource_type.re_type_name, resource_type.re_type_code, 
        resource.re_image, resource.re_yearbook, 
        resource_category.re_cate_code, resource_category.re_cate_name, 
        resource_language.re_lang_name 
        FROM resource
        LEFT JOIN resource_type ON resource.re_type = resource_type.re_type_id 
		LEFT JOIN resource_category ON resource.re_category = resource_category.re_cate_id 
        LEFT JOIN resource_language ON resource.re_language = resource_language.re_lang_id 
        WHERE resource.re_code = '$re_code'");
        $strReso->execute();
        $resultstrReso = $strReso->fetchAll(PDO::FETCH_OBJ);

        if(!isset($resultstrReso[0]->re_code)){
            $error_r = "1";
            $error_description = "ไม่มีทรัพยากรนี้ในระบบ";
        }else{
            $error_r = "0";
            $error_description = "";
        }
        
        $selectCheckBorrow = $this->container->db->query("SELECT 
        status_resource,id_borrow 
        FROM borrow 
        WHERE resource_code = '$re_code' ORDER BY id_borrow DESC LIMIT 1");
        $selectCheckBorrow->execute();
        $resultselectCheckBorrow = $selectCheckBorrow->fetchAll(PDO::FETCH_OBJ);

        if(!isset($resultstrReso[0]->re_code)){
            $error_r = "1";
            $error_description = "ไม่มีทรัพยากรนี้ในระบบ";
        }else{
            $error_r = "0";
            $error_description = "";
            if($resultselectCheckBorrow[0]->status_resource == "" || $resultselectCheckBorrow[0]->status_resource == "0"){
                $chk_checkout = "true";
            }else{
                $chk_checkout =  "false";
            }
        }
        
        $temparray = array();
        $temparray[] = array(
            'error' => $error_r, 
            'error_description' => $error_description, 
            'resource_type' => $resultstrReso[0]->re_type_name, 
            'resource_typecode' => $resultstrReso[0]->re_type_code, 
            'barcode' => $re_code, 
            'chk_checkout' => $chk_checkout, 
            'media_name' => $resultstrReso[0]->re_title, 
            'media_auther' => $resultstrReso[0]->re_auther, 
            'media_detail' => $resultstrReso[0]->re_topic, 
            'price' => $resultstrReso[0]->re_note, 
            'isbn' => $resultstrReso[0]->re_isbn, 
            're_image' => $resultstrReso[0]->re_image, 
            'yearbook' => $resultstrReso[0]->re_yearbook, 
            'category_code' => $resultstrReso[0]->re_cate_code, 
            'category' => $resultstrReso[0]->re_cate_name, 
            'language' => $resultstrReso[0]->re_lang_name

        );

        $response = $this->response->withJson($temparray);
        return $response;
        
    }
    
}