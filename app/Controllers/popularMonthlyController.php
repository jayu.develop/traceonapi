<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use PDO;

class popularMonthlyController extends Controller
{
	public function popularMonthly(ServerRequestInterface $request, ResponseInterface $response)
    {
        error_reporting(E_ALL & ~E_NOTICE);
        $temparray = array();
        $sql_monthly = $this->container->db->query("SELECT 
        resource_code ,count_borrow ,re_image
        FROM view_popular_weekly");
        $sql_monthly->execute();
        
        foreach($resultsql_monthly = $sql_monthly->fetchAll(PDO::FETCH_OBJ) AS $row) {

            $temparray[] = array(
                'resource_code' => $row->resource_code, 
                'count_borrow' => $row->count_borrow,
                're_image' => $row->re_image
            );

        }
        
        $response = $this->response->withJson($temparray);
        return $response;

    }
}