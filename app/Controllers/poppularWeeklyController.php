<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use PDO;

class poppularWeeklyController extends Controller
{
	public function poppularWeekly(ServerRequestInterface $request, ResponseInterface $response)
    {
        error_reporting(E_ALL & ~E_NOTICE);
        $temparray = array();
        $sql_weekly = $this->container->db->query("SELECT 
        resource_code ,count_borrow ,re_image
        FROM view_popular_weekly");
        $sql_weekly->execute();
        
        foreach($resultsql_weekly = $sql_weekly->fetchAll(PDO::FETCH_OBJ) AS $row) {

            $temparray[] = array(
                'resource_code' => $row->resource_code, 
                'count_borrow' => $row->count_borrow,
                're_image' => $row->re_image
            );

        }
        
        $response = $this->response->withJson($temparray);
        return $response;

    }
}