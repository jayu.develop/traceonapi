<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use PDO;

class checkinController extends Controller
{
	public function checkin(ServerRequestInterface $request, ResponseInterface $response)
    {
        error_reporting(E_ALL & ~E_NOTICE);
        $re_code = $request->getAttribute('re_code');
        $strReso = $this->container->db->query("SELECT 
        re_code, re_title, re_image FROM resource 
        WHERE re_code = '$re_code'");
        $strReso->execute();
        $resultstrReso = $strReso->fetchAll(PDO::FETCH_OBJ);

        $strCheckin = $this->container->db->query("SELECT 
        borrow.id_borrow, borrow.status_resource, borrow.date_end, 
        member.member_name, member.member_lastname 
        FROM borrow 
        LEFT JOIN member ON borrow.member_id = member.member_id 
        WHERE borrow.resource_code = '$re_code' ORDER BY borrow.id_borrow DESC LIMIT 1");
        $strCheckin->execute();
        $resultstrCheckin = $strCheckin->fetchAll(PDO::FETCH_OBJ);

        if($strCheckin){
            if(!isset($resultstrReso[0]->re_code)){
                $error_r = "1";
                $error_description = "ไม่มีทรัพยากรนี้ในระบบ";
                $status = "false";
                $media_name = $resultstrReso[0]->re_title;
                $image = $resultstrReso[0]->re_image;
                $media_code = $re_code;
            }else{
                if(!isset($resultstrCheckin) || $resultstrCheckin[0]->status_resource == "0"){
                    $error_r = "1";
                    $error_description = "หนังสือไม่ได้ถูกยืม";
                    $status = "false";
                    $media_name = $resultstrReso[0]->re_title;
                    $image = $resultstrReso[0]->re_image;
                    $media_code = $re_code;
                }else{
                    $error_r = "0";
                    $media_name = $resultstrReso[0]->re_title;
                    $image = $resultstrReso[0]->re_image;
                    $media_code = $re_code;
                    $member_name = $resultstrCheckin[0]->member_name." ".$resultstrCheckin[0]->member_lastname;
                    $status = "success";

                    $strCheckin = $this->container->db->query("UPDATE borrow 
                    SET status_resource = '0', date_return = CURDATE() 
                    WHERE resource_code = '$re_code' AND status_resource = '1' ORDER BY id_borrow DESC LIMIT 1");
                    $strCheckin->execute();

                        if(date('Y-m-d') > $resultstrCheckin[0]->date_end){
                            $strCheckin = $this->container->db->query("UPDATE borrow 
                            SET status_fine = '1' 
                            WHERE id_borrow = '".$resultstrCheckin[0]->id_borrow."'");
                            $strCheckin->execute();
                        }

                }
            }
        }else{
            $error_r = "1";
            $error_description = "";
            $status = "false"; 
        }
     
        $temparray = array();
        $temparray[] = array(
            'error' => $error_r, 
            'error_description' => $error_description, 
            'media_name' => $media_name, 
            'media_code' => $media_code, 
            'member_name' => $member_name, 
            'image' => $image, 
            'status' => $status
            
        );

        $response = $this->response->withJson($temparray);
        return $response;   

    }
}