<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;

use PDO;

class checkout_againController extends Controller
{

    public function DateDiff($strDate1,$strDate2)
    {
        return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );
    }

    public function checkout_again(ServerRequestInterface $request, ResponseInterface $response)
    {   
        error_reporting(E_ALL & ~E_NOTICE);
        //strMem
        $member_cardID = $request->getAttribute('member_cardID');
        $re_code = $request->getAttribute('re_code');

        $strMem = $this->container->db->query("SELECT member.member_id,member.member_status, member.member_expire, privilege.number_day
        FROM member 
        INNER JOIN privilege ON member.member_type = privilege.member_type 
        WHERE member.member_cardID = '$member_cardID'");
        $strMem->execute();
        $resultstrMem = $strMem->fetchAll(PDO::FETCH_OBJ);

        $selectCheckBorrow = $this->container->db->query("SELECT status_resource,id_borrow FROM borrow 
        WHERE resource_code = '$re_code' ORDER BY id_borrow DESC LIMIT 1");
        $selectCheckBorrow->execute();
        $resultCheckBorrow = $selectCheckBorrow->fetchAll(PDO::FETCH_OBJ);

        $strResource = $this->container->db->query("SELECT re_title, re_code, re_image FROM resource 
        WHERE re_code = '$re_code'");
        $strResource->execute();
        $resultResource = $strResource->fetchALL(PDO::FETCH_OBJ);

        $strResou = $this->container->db->query("SELECT date_end, count_borrow, member_id, id_borrow FROM borrow 
        WHERE resource_code = '$re_code' AND status_resource = '1'");
        $strResou->execute();
        $resultResou = $strResou->fetchALL(PDO::FETCH_OBJ);

        if(!empty($_SESSION['errors'])){ // close Notice
        $countResou = $resultResou[0]->count_borrow + 1;
        $checkDate = date ("Y-m-d", strtotime("-2 day", strtotime($resultResou[0]->date_end)));
        }

        $strHold = $this->container->db->query("SELECT resource_code FROM hold 
        WHERE resource_code = '$re_code' AND date_hold = CURDATE()");
        $strHold->execute();
        $resultHold = $strHold->fetchALL(PDO::FETCH_OBJ);

        //ตัวแปรค่าปรับ
        $strSQLFine = $this->container->db->query("SELECT rate_fine FROM rate_fine");
        $strSQLFine->execute();
        $resultSQLFine = $strSQLFine->fetchAll(PDO::FETCH_OBJ);
        $rateFine = $resultSQLFine[0]->rate_fine;

        //ค่าปรับ
        $strBorrow = $this->container->db->query("SELECT date_return, date_end, status_fine  FROM borrow 
        WHERE member_id = '".$resultstrMem[0]->member_id."' AND (status_resource = '1' OR status_fine = '1') 
        AND  date_end < CURDATE()");
        $strBorrow->execute();
        $sumFine = 0;
        foreach($resultBorrow = $strBorrow->fetchALL(PDO::FETCH_OBJ) AS $row){
            if($row->date_return != '0000-00-00' && $row->status_fine == '1'){
                $dateDiff = $this->DateDiff($row->date_end,$row->date_return);
                $valueFine = $dateDiff * $rateFine;
            }else{							  
                $dateDiff = $this->DateDiff($row->date_end, date('Y-m-d'));
                $valueFine = $dateDiff * $rateFine;                                        
            } 
            $sumFine += $valueFine;
        }

        $due = date("Y-m-d", mktime(date("H")+0, date("i")+0, date("s")+0, date("m")+0 , date("d")+$resultstrMem[0]->number_day +2, date("Y")+0));
        $expire = $this->DateDiff(date('Y-m-d'), $resultstrMem[0]->member_expire);

        if(isset($resultResource) && isset($resultstrMem) && $resultstrMem[0]->member_status == '1' && $expire > '30' 
            && ($sumFine == '' || $sumFine == '0') && $resultCheckBorrow[0]->status_resource == '1'
            && $resultResou[0]->member_id == $resultstrMem[0]->member_id && date('Y-m-d') == $checkDate 
            && $resultHold[0]->resource_code == "" && $resultResou[0]->count_borrow <= "1")
        {
            $sqlBorrow = $this->container->db->query("UPDATE borrow SET date_end = '".$due."', count_borrow = '".$countResou."'
                WHERE id_borrow = '".$resultResou[0]->id_borrow."' AND resource_code = '$re_code'");
                $sqlBorrow->execute();;
                            
                $status = "success";
            
            }else{
    
                $status = "false";

            }

            if($status == "success"){

                $temparray = array();
                $temparray[] = array('error' => 0, 
                    'error_description' => "",
                    'media_name' => $resultResource[0]->re_title,
                    'media_code' => $resultResource[0]->re_code,
                    're_image' => $resultResource[0]->re_image,
                    'due' => $due,
                    'status' => $status
                    );
                    
                $response = $this->response->withJson($temparray);
                return $response;

            }

            else if($status == "false")
            
            {
            
                if(!isset($resultstrMem)){ $error_description = "ไม่มีข้อมูลผู้ใช้ในระบบ"; }
                else if($resultstrMem[0]->member_status == '0'){ $error_description = "สมาชิกโดนระงับการใช้งาน"; }
                else if($expire < '0'){ $error_description = "บัตรหมดอายุ"; }
                else if($expire <= '30'){ $error_description = "บัตรใกล้หมดอายุ"; }
                else if($sumFine > '0'){ $error_description = "มีค่าปรับคงค้าง"; }
                else if(!isset($resultResource)){ $error_description = "ไม่มีทรัพยากรในระบบ"; }
                else if($resultCheckBorrow[0]->status_resource != '1'){ $error_description = "หนังสือไม่ได้ถูกยืม"; }
                else if($resultResou[0]->member_id != $resultstrMem[0]->member_id){ $error_description = "ข้อมูลผู้ยืมไม่ตรงกัน"; }
                else if(date('Y-m-d') != $checkDate){ $error_description = "โปรดยืมต่อในช่วงระยะเวลาที่กำหนด";}
                else if($resultHold[0]->resource_code != ""){ $error_description = "มีการจองทรัพยากร"; }
                else if($resultResou[0]->count_borrow > "1"){ $error_description = "ยืมต่อเกินกำหนด"; }
            
                $temparray = array();
                $temparray[] = array('error' => 1, 
                    'error_description' => $error_description,
                    'media_name' => $resultResource[0]->re_title,
                    'media_code' => $resultResource[0]->re_code,
                    're_image' => $resultResource[0]->re_image
                    );

                $response = $this->response->withJson($temparray);
                return $response;						
            
            }




    }
}