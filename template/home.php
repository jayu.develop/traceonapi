<!DOCTYPE html>
<?php error_reporting(0);?>

<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>TRACEON API</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>

    <body>

    <style>
        .text-muted { margin-left: 10px; }
        h2 { margin-bottom: 20px; }
    </style>
        
    <div class="container"><!-- container -->

    <section>
    <div class="panel panel-default">
    <div class="panel-body">
        <form action="{{ path_for('status') }}" class="form-inline text-center" method="get">
            <h2 class="text-primary">TRACEON <b>API</b></h2>

            <div class="form-row align-items-center">
                <div class="form-group">
                    <label for="" class="text-muted">COMMAND : </label>

                    <select class="form-control" id="command" name="command">
                        <option <?php if($_GET["command"] == 'checkpatron'){ echo "selected";} ?> value="checkpatron">Checkpatron</option>
                        <option <?php if($_GET["command"] == 'checkout'){ echo "selected";} ?> value="checkout">Checkout</option>
                        <option <?php if($_GET["command"] == 'checkout_again'){ echo "selected";} ?> value="checkout_again">Checkout Again</option>
                        <option <?php if($_GET["command"] == 'checkin'){ echo "selected";} ?> value="checkin">Checkin</option>
                        <option <?php if($_GET["command"] == 'checkstatus'){ echo "selected";} ?> value="checkstatus">Checkstatus</option>
                        <option <?php if($_GET["command"] == 'popularWeekly'){ echo "selected";} ?> value="popularWeekly">Popular Weekly</option>
                        <option <?php if($_GET["command"] == 'popularMonthly'){ echo "selected";} ?> value="popularMonthly">Popular Monthly</option>
                        <option <?php if($_GET["command"] == 'justReturn'){ echo "selected";} ?> value="justReturn">Just Return</option>
                        <option <?php if($_GET["command"] == 'newReleases'){ echo "selected";} ?> value="newReleases">New Releases</option>
                    </select>
                  
                </div>
                <div class="form-group">
                    <label for="" class="text-muted">PATRON ID : </label> 
                    <input type="text" class="form-control" name="PatronID" value="<?php echo $_GET["PatronID"];?>">
                </div>
                <div class="form-group">
                    <label for="" class="text-muted">BARCODE : </label>  
                    <input type="text" class="form-control" name="Barcode" value="<?php echo $_GET["Barcode"];?>">
                </div>

                <input type="submit" class="btn btn-info" name="submit" value="Send">
            </div>

        </form>
    </div>
    </div>
    </section>

    <div>
        <a href="{{ path_for('teststatus') }}">Home</a>
    </div>


    <?php
        
    ?>




    </div><!-- container -->


    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>

                  