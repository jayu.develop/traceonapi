<?php

session_start();

require __DIR__ . '/../vendor/autoload.php';


class Database
{
	private $pdo;
	
	public function __construct(PDO $pdo)
	{
		$this->pdo = $pdo;
	}

	public function query($sql)
	{
		return $this->pdo->prepare($sql);
	}
}

$app = new \Slim\App([
	'settings' => [
		'displayErrorDetails' => true,
	],
	
]);

$container = $app->getContainer();

$container['pdo'] = function () {

	$dbhost = "mysql";
	$dbusername = "admindb";
	$dbpassword = "adminpass";
	$dbname = "event_hongkhai_library";

	$pdo = new PDO("mysql:host=". $dbhost .";dbname=". $dbname.";charset=utf8", $dbusername, $dbpassword);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $pdo;
};

/*
$container['pdo'] = function () {
	
		$dbhost = "localhost";
		$dbusername = "traceon";
		$dbpassword = "Tra1220!";
		$dbname = "traceon_library";
	
		$pdo = new PDO("mysql:host=". $dbhost .";dbname=". $dbname.";charset=utf8", $dbusername, $dbpassword);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $pdo;
};
*/
$container['db'] = function ($container) {
	return new Database($container->pdo);
};

$container['view'] = function ($container) {
	$view = new \Slim\Views\Twig(__DIR__ . '/../resources/views', [
		'cache' => false,
	]);

	$view->addExtension(new \Slim\Views\TwigExtension(
		$container->router,
		$container->request->getUri()
	));

	return $view;
};

$container['HomeController'] = function ($container) {
	return new \App\Controllers\HomeController($container);
};

$container['checkstatusController'] = function ($container) {
	return new \App\Controllers\checkstatusController($container);
};

$container['checkpatronController'] = function ($container) {
	return new \App\Controllers\checkpatronController($container);
};

$container['checkinController'] = function ($container) {
	return new \App\Controllers\checkinController($container);
};

$container['checkoutController'] = function ($container) {
	return new \App\Controllers\checkoutController($container);
};

$container['poppularWeeklyController'] = function ($container) {
	return new \App\Controllers\poppularWeeklyController($container);
};

$container['popularMonthlyController'] = function ($container) {
	return new \App\Controllers\popularMonthlyController($container);
};

$container['newReleasesController'] = function ($container) {
	return new \App\Controllers\newReleasesController($container);
};

$container['justReturnController'] = function ($container) {
	return new \App\Controllers\justReturnController($container);
};

$container['checkout_againController'] = function ($container) {
	return new \App\Controllers\checkout_againController($container);
};

$container['testController'] = function ($container) {
	return new \App\Controllers\testController($container);
};


require __DIR__ . '/../app/routes.php';
